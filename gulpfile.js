const pug = require('gulp-pug');
const stylus = require('gulp-stylus');
const {src, dest, parallel, watch, series} = require('gulp');
const browserSync = require('browser-sync').create();

const bs = (done) => {
    browserSync.init({
        server: "./dest",
    });
    browserSync.watch('dest/').on('change', browserSync.reload);
    done();
}

const pugTask = () => {
    return src('src/*.pug')
        .pipe(pug())
        .on('error', console.log)
        .pipe(dest('dest/'));
}

const assetsTask = () => {
    return src(['src/*.svg', "src/*.png", "src/*.jpg", "src/*.jpeg", "src/*.gif"])
        .pipe(dest('dest/'));
}

const stylusTask = () => {
    return src('src/*.styl')
        .pipe(stylus())
        .on('error', console.log)
        .pipe(dest('dest/'));
}

const pugWatcher = watch(['src/*.pug']);
const stylusWatcher = watch(['src/*.styl']);

pugWatcher.on('change', pugTask);
stylusWatcher.on('change', stylusTask);

exports.default = series(parallel(pugTask, stylusTask, assetsTask), bs);
